const functions = require('./functions');

const initDatabase = () => console.log('Database Initialized...');
const closeDatabase = () => console.log('Database Closed...');

// run every test
// beforeEach(() => initDatabase());
// afterEach(() => closeDatabase());

// only run once 
beforeAll(() => initDatabase());
afterAll(() => closeDatabase());

test('Adds 2 + 2 to equal 4', () => {
  expect(functions.add(2, 2)).toBe(4);
});

test('Adds 2 + 2 to NOT equal 5', () => {
  expect(functions.add(2, 2)).not.toBe(5);
});

test('Should be null', () => {
  expect(functions.isNull()).toBeNull();
});

test('Should be falsy', () => {
  expect(functions.checkValue(null)).toBeFalsy();
  expect(functions.checkValue(0)).toBeFalsy();
  expect(functions.checkValue(undefined)).toBeFalsy();
  expect(functions.checkValue(false)).toBeFalsy();
});

test('User should be Iman Syaefulloh object', () => {
  expect(functions.createUser()).toEqual({
    firstName: 'Iman',
    lastName: 'Syaefulloh'
  });
});

test('should be under 1600', () => {
  const load1 = 800;
  const load2 = 700;
  expect(load1 + load2).toBeLessThan(1600);
});

test('there is no I in team', () => {
  expect('team').not.toMatch(/I/i);
});

test('admin should be in usernames array', () => {
  let usernames = ['john', 'karen', 'admin'];
  expect(usernames).toContain('admin');
});

test('user fetched name should be Leanne Graham', () => {
  return functions.fetchUser()
    .then(data => {
      expect(data.name).toEqual('Leanne Graham');
    });
});

test('user fetched name should be Leanne Graham [async await]', async () => {
  const data = await functions.fetchUser();
  expect(data.name).toEqual('Leanne Graham')
});